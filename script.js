const $block =  document.querySelector('p'),
	  $list = document.querySelector('ul');
let indexEl = 0,
	removeEl;
$list.addEventListener('click',clickList);
document.addEventListener('click',outClickList);
function clickList(e){
	let clickFild = e.target,
		color = clickFild.getAttribute('data-color');
	if(removeEl){
		$list.insertBefore(removeEl,$list.children[indexEl]);
	}
	for(let i=0;i<$list.children.length;i++){
		if($list.children[i].getAttribute('data-color') === color){
			indexEl = i;
		} 
	}
	removeEl = $list.removeChild(clickFild);
	$block.innerHTML = clickFild.innerText;
	$block.style.background = color;
	console.log($list);
};

function outClickList (e){
	if(e.target.tagName !== 'LI' && removeEl){
		$list.insertBefore(removeEl,$list.children[indexEl]);
		$block.innerHTML = '';
		$block.style.background = '';
	}
};


